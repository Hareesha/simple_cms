class SubjectsController < ApplicationController
  layout "admin"
  before_action :confirm_logged_in
  def index
     @subjects = Subject.sorted
  end

  def show
    @subject = Subject.find(params[:id])
  end

  def new
     @subject = Subject.new({:name => "Default"})
  end
  def create
   #Instantiate the New Object using Form parameters
    @subject =Subject.new(subject_params)
   #Save the new Object
   if @subject.save
       flash[:notice] = "The Subject was created successfully" 
       redirect_to(:action => 'index')
   else
      render('new')
   end
 end
  def edit
   @subject = Subject.find(params[:id])
  end
  def update
  # Find an Existing object using form parameters
   @subject = Subject.find(params[:id])
 
      #Update the Object
    if @subject.update_attributes(subject_params) 
        flash[:notice]="The Subject Updated successfully"
        redirect_to(:action => 'show',:id => @subject.id)
    else
       render('edit')
   end
  end
  def delete
   @subject = Subject.find(params[:id])
  end
  def destroy 
    subject = Subject.find(params[:id])
    subject.destroy
    flash[:notice] = "Subject '#{subject.name}' Deleted successfully"
     redirect_to(:action => 'index')
   
  end
 private
  def subject_params
    # This is same as using "params[:subject]"except itraises an error if :subj     #  ect is not present allows listed attributes to be mass assignment
    params.require(:subject).permit(:name,:position,:visible)
  end
end
