class SectionsController < ApplicationController
 layout "admin"
 before_action :confirm_logged_in
 def index
  @sections=Section.all
 end
 def show
 @section=Section.find(params[:id])   
 end
 def new
 @section=Section.new
 end
 def create
  @section=Section.new(sections_params)
   if @section.save
     flash[:notice]="Section Created Successfully"
     redirect_to({:action => "index"})
   else
     render('new')
   end
 end
 def edit
  @section = Section.find(params['id'])
end
 def update
  @section=Section.find(params[:id])
 if  @section.update_attributes(sections_params)
  flash[:notice] = "Section is Updated Successfully"
  redirect_to({:action => 'show',:id => @section.id })
 else 
    render('edit')
  end
  end
  def delete
   @section = Section.find(params['id']) 
  end
  def destroy
    section= Section.find(params[:id]).destroy
    flash[:notice] = "Section destroy Successfully"
    redirect_to(:action => 'index')
  end 
 


private
 def sections_params
  params.require(:section).permit(:page_id,:name,:position,:visible,:content_type,:content)
end
end
