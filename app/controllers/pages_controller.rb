class PagesController < ApplicationController
  layout "admin"
  before_action :confirm_logged_in
 def index
    @pages=Page.all
 end

 def new
     @page=Page.new  
 end
 
 def create
   @page=Page.new(pages_params)
   puts @page.name
   if @page.save
      flash[:notice] = "New Page Details to the Database"
      redirect_to(:action => "index")
   else
      render('new')   
   end
 end
 def show
     @page=Page.find(params[:id])
 end
 def edit
   @page=Page.find(params[:id])
 end
 def update
    @page=Page.find(params[:id])
    puts @page.name
    if @page.update_attributes(pages_params)
       flash[:notice]="Page is Updated successfully"
       redirect_to({:action => 'show',:id => @page.id})
    else
      render('edit')
   end
 end
 def delete
  @page=Page.find(params[:id])
 end
 def destroy
     page=Page.find(params[:id])
     puts page.name
     page.destroy
     flash[:notice] = "Page #{page.name} Deleted Suceessfully"
     redirect_to({:action => 'index'})
    
 end
 private
   def pages_params
       params.require(:page).permit(:subject_id,:name,:permalink,:position,:visible)
   end
end
