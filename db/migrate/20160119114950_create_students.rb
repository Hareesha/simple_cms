class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string "name"
      t.integer "position"
      t.boolean "visible", :default => false
      t.timestamps null: false
    end
  end
end
